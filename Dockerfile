# Etapa de construcción
FROM nginx:latest as builder
COPY . /usr/share/nginx/html

# Etapa de producción
FROM nginx:latest
COPY --from=builder /usr/share/nginx/html /usr/share/nginx/html


FROM amazon/aws-cli

WORKDIR /aws

ENTRYPOINT ["aws"]
